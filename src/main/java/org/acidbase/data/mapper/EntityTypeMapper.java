package org.acidbase.data.mapper;

import org.acidbase.data.EntityType;
import org.acidbase.data.ServerInfo;
import org.json.simple.parser.ParseException;
import org.postgresql.util.PGobject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

public class EntityTypeMapper extends Mapper
{
    public EntityTypeMapper(Connector connector)
    {
        super(connector);
    }

    public Collection<EntityType> getEntityType()
        throws SQLException, ParseException
    {
        List<Map<String, Object>> data = query("SELECT name, structure FROM \"Acidbase\".\"entity_structure\" ORDER BY name");

        Map<String, EntityType> _types = new TreeMap<>();
        for (Map<String, Object> rec : data) {
            EntityType et = new EntityType(rec.get("name").toString(), EntityType.parseFields(rec.get("structure").toString()));
            _types.put(rec.get("name").toString(), et);
        }
        return _types.values();
    }

    public boolean addEntityType(EntityType type)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("INSERT INTO \"Acidbase\".\"entity_structure\" (name, structure) VALUES (?, ?)");
            pst.setString(1, type.getName());
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(EntityType.stringifyFields(type.getFields()));
            pst.setObject(2, pgo);
            pst.executeUpdate();
            int count = pst.getUpdateCount();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public boolean removeEntityType(String name)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("DELETE FROM \"Acidbase\".\"entity_structure\" WHERE name = ?");
            pst.setString(1, name);
            pst.executeUpdate();
            int count = pst.getUpdateCount();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public boolean updateEntityType(String name, EntityType type)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("UPDATE \"Acidbase\".\"entity_structure\" SET name = ?, structure = ? WHERE name = ?");
            pst.setString(1, type.getName());
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(EntityType.stringifyFields(type.getFields()));
            pst.setObject(2, pgo);
            pst.setString(3, name);
            pst.executeUpdate();
            int count = pst.getUpdateCount();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }
}
