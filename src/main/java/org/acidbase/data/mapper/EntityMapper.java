package org.acidbase.data.mapper;

import org.acidbase.data.Entity;
import org.acidbase.data.EntityInfo;
import org.acidbase.data.structure.ReadingStructure;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.postgresql.util.PGobject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EntityMapper extends Mapper
{
    public EntityMapper(Connector connector)
    {
        super(connector);
    }

    public EntityInfo storeEntity(String entity_type, Entity data, Long user_id)
        throws SQLException
    {
        return storeEntity(entity_type, data, user_id, null);
    }

    public EntityInfo storeEntity(String entity_type, Entity data, Long user_id, Long entity_id)
        throws SQLException
    {
        Integer key = -1;
        String sql;
        if (entity_id != null) {
            sql = "SELECT * FROM \"Acidbase\".store_entity(?, ?, ?, ?)";
        }
        else {
            sql = "SELECT * FROM \"Acidbase\".new_entity(?, ?, ?)";
        }
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement(sql);

            if (entity_id != null) {
                pst.setLong(1, entity_id);
                pst.setString(2, entity_type);
                pst.setLong(3, user_id);
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(data.stringifyForDb());
                pst.setObject(4, pgo);
            }
            else {
                pst.setString(1, entity_type);
                pst.setLong(2, user_id);
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(data.stringifyForDb());
                pst.setObject(3, pgo);
            }

            ResultSet rs = pst.executeQuery();
            EntityInfo ei = null;
            if (rs.next()) {
                ei = new EntityInfo();
                ei.entity_id = rs.getLong("entity_id");
                ei.revision_id = rs.getLong("revision_id");
                ei.entity_type = rs.getString("entity_type");
                ei.create_date = rs.getTimestamp("create_date");
                ei.revise_date = rs.getTimestamp("revise_date");
                ei.creator = rs.getLong("creator");
                ei.reviser = rs.getLong("reviser");
                ei.revision_state = string2RevisionState(rs.getString("revision_state"));
            }
            rs.close();
            pst.close();
            commit(key);
            return ei;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public Entity getEntity(Long entity_id, ReadingStructure readingStructure, ExtractionState state, LockType lock_type)
        throws SQLException, ParseException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            PreparedStatement pst = con.prepareStatement("SELECT * FROM \"Acidbase\".extract_entity(?, ?, ?)");
            pst.setLong(1, entity_id);
            PGobject pgo = new PGobject();
            pgo.setType("jsonb");
            pgo.setValue(readingStructure.toJson());
            pst.setObject(2, pgo);
            pst.setObject(3, extractionState2String(state), Types.OTHER);

            ResultSet rs = pst.executeQuery();
            String str = null;
            if (rs.next()) {
                str = rs.getString(1);
            }
            rs.close();
            pst.close();

            commit(key);
            return Entity.parse(str);
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public String getObjectStructure(Long object_id)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("SELECT * FROM \"Acidbase\".extract_object_forward_structure(?)");
            pst.setLong(1, object_id);
            ResultSet rs = pst.executeQuery();
            String str = null;
            if (rs.next()) {
                str = rs.getString(1);
            }
            rs.close();
            pst.close();
            commit(key);
            return str;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public Entity getEntityByRevision(Long revision_id, ReadingStructure readingStructure, ExtractionState state, LockType lock_type)
        throws SQLException, ParseException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            PreparedStatement pst = con.prepareStatement("SELECT * FROM \"Acidbase\".extract_entity_by_revision(?, ?, ?)");
            pst.setLong(1, revision_id);
            PGobject pgo = new PGobject();
            pgo.setType("jsonb");
            pgo.setValue(readingStructure.toJson());
            pst.setObject(2, pgo);
            pst.setObject(3, extractionState2String(state), Types.OTHER);

            ResultSet rs = pst.executeQuery();
            String str = null;
            if (rs.next()) {
                str = rs.getString(1);
            }
            rs.close();
            pst.close();

            commit(key);
            return Entity.parse(str);
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public Entity getRevision(Long object_id, ReadingStructure readingStructure, LockType lock_type)
        throws SQLException, ParseException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            PreparedStatement pst = con.prepareStatement("SELECT * FROM \"Acidbase\".extract_revision(?, ?)");
            pst.setLong(1, object_id);
            PGobject pgo = new PGobject();
            pgo.setType("jsonb");
            pgo.setValue(readingStructure.toJson());
            pst.setObject(2, pgo);

            ResultSet rs = pst.executeQuery();
            String str = null;
            if (rs.next()) {
                str = rs.getString(1);
            }
            rs.close();
            pst.close();

            commit(key);
            return Entity.parse(str);
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public List<EntityInfo> getRelatedRevisions(Long entity_id, LockType lock_type)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            List<EntityInfo> eis = new ArrayList<>();

            //Forward related
            {
                String sql =
                    "SELECT r2.entity_id, r2.object_id AS revision_id, e.entity_type, e.create_date, r2.revise_date, e.creator, r2.reviser, r2.revision_state, r2.fields " +
                    "FROM \"Acidbase\".revision r " +
                    "INNER JOIN \"Acidbase\".collection c ON (r.object_id = c.object_id) " +
                    "INNER JOIN \"Acidbase\".item i ON (c.collection_id = i.collection_id) " +
                    "INNER JOIN \"Acidbase\".revision r2 ON (i.revision_id = r2.object_id) " +
                    "INNER JOIN \"Acidbase\".entity e ON (r2.entity_id = e.entity_id) " +
                    "WHERE " +
                    "    r.entity_id = ? ";

                switch (lock_type) {
                    case Exclusive:
                        sql += "FOR UPDATE;";
                        break;

                    case Shared:
                        sql += "FOR SHARE;";
                        break;

                    case NoLock:
                        sql += ";";
                        break;
                }

                PreparedStatement pst = con.prepareStatement(sql);
                pst.setLong(1, entity_id);

                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    eis.add(instantiateEntityInfo(rs));
                }
                rs.close();
                pst.close();
            }

            //Reverse related
            {
                String sql =
                    "SELECT r2.entity_id, r2.object_id AS revision_id, e.entity_type, e.create_date, r2.revise_date, e.creator, r2.reviser, r2.revision_state, r2.fields " +
                    "FROM \"Acidbase\".revision r " +
                    "INNER JOIN \"Acidbase\".item i ON (r.object_id = i.revision_id) " +
                    "INNER JOIN \"Acidbase\".revision r2 ON (i.object_id = r2.object_id) " +
                    "INNER JOIN \"Acidbase\".entity e ON (r2.entity_id = e.entity_id) " +
                    "WHERE " +
                    "    r.entity_id = ? ";

                switch (lock_type) {
                    case Exclusive:
                        sql += "FOR UPDATE;";
                        break;

                    case Shared:
                        sql += "FOR SHARE;";
                        break;

                    case NoLock:
                        sql += ";";
                        break;
                }

                PreparedStatement pst = con.prepareStatement(sql);
                pst.setLong(1, entity_id);

                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
                    eis.add(instantiateEntityInfo(rs));
                }
                rs.close();
                pst.close();
            }

            List<EntityInfo> uniqueList = new ArrayList<>();
            for (EntityInfo ei : eis) {
                boolean found = false;
                for (EntityInfo ei2 : uniqueList) {
                    if (ei.revision_id.equals(ei2.revision_id)) {
                        found = true;
                    }
                }
                if (!found) {
                    uniqueList.add(ei);
                }
            }

            commit(key);
            return uniqueList;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public void activateExternalIndexing()
        throws SQLException
    {
        activateExternalIndexing(true);
    }

    public void activateExternalIndexing(boolean activate)
        throws SQLException
    {
        if (activate) {
            query("SELECT set_config('acidbase.external_indexing', 'true', false);");
        }
        else {
            query("SELECT set_config('acidbase.external_indexing', 'false', false);");
        }
    }

    public List<EntityInfo> listEntities(String entity_type, Long start, Long limit)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            String query =
                "SELECT " +
                "   e.entity_id, r.object_id AS revision_id, e.entity_type, r.revision_state, e.create_date, e.creator, r.revise_date, r.reviser, r.fields " +
                "FROM \"Acidbase\".entity e " +
                "INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) " +
                "WHERE r.revision_state = 'Active' "
            ;
            if (entity_type != null) {
                query += " AND e.entity_type = ? ";
            }
            if (start != null) {
                query += " LIMIT ? OFFSET ?";
            }

            PreparedStatement pst = con.prepareStatement(query);

            int paramCounter = 0;
            if (entity_type != null) {
                pst.setString(++paramCounter, entity_type);
            }
            if (start != null) {
                pst.setLong(++paramCounter, limit);
                pst.setLong(++paramCounter, start);
            }
            ResultSet rs = pst.executeQuery();
            ArrayList<EntityInfo> entities = new ArrayList<>();
            while (rs.next()) {
                EntityInfo ei = instantiateEntityInfo(rs);
                entities.add(ei);
            }
            rs.close();
            pst.close();
            commit(key);
            return entities;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public long countEntities(String entity_type)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            String query =
                "SELECT " +
                "   COUNT(*) AS count " +
                "FROM \"Acidbase\".entity e " +
                "INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) " +
                "WHERE r.revision_state = 'Active' "
                ;
            if (entity_type != null) {
                query += " AND e.entity_type = ? ";
            }

            PreparedStatement pst = con.prepareStatement(query);

            if (entity_type != null) {
                pst.setString(1, entity_type);
            }

            ResultSet rs = pst.executeQuery();
            rs.next();
            long count = rs.getLong(1);
            rs.close();
            pst.close();
            commit(key);
            return count;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public EntityInfo getLatestEntityInfo(Long entity_id)
        throws SQLException
    {
        String sql =
            "SELECT r.object_id AS revision_id, r.entity_id, e.entity_type, r.revise_date, e.create_date, r.reviser, e.creator, r.revision_state, r.fields " +
            "FROM \"Acidbase\".entity e " +
            "INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) " +
            "WHERE e.entity_id = ? AND r.revision_state IN ('Active', 'Deleted') ";

        return _getLatestEntityInfo(sql, entity_id);
    }

    public EntityInfo getLatestEntityInfoByRevisionId(Long revision_id)
        throws SQLException
    {
        String sql =
            "SELECT r.object_id AS revision_id, r.entity_id, e.entity_type, r.revise_date, e.create_date, r.reviser, e.creator, r.revision_state, r.fields " +
            "FROM \"Acidbase\".revision _r " +
            "INNER JOIN \"Acidbase\".revision r ON (_r.entity_id = r.entity_id) " +
            "INNER JOIN \"Acidbase\".entity e ON (r.entity_id = e.entity_id) " +
            "WHERE _r.object_id = ? AND r.revision_state IN ('Active', 'Deleted') ";

        return _getLatestEntityInfo(sql, revision_id);
    }

    public EntityInfo getRevisionEntityInfo(Long revision_id)
        throws SQLException
    {
        String sql =
            "SELECT r.object_id AS revision_id, r.entity_id, e.entity_type, r.revise_date, e.create_date, r.reviser, e.creator, r.revision_state, r.fields " +
            "FROM \"Acidbase\".revision r " +
            "INNER JOIN \"Acidbase\".entity e ON (r.entity_id = e.entity_id) " +
            "WHERE r.object_id = ? ";

        return _getLatestEntityInfo(sql, revision_id);
    }

    private EntityInfo _getLatestEntityInfo(String sql, Long id)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setLong(1, id);
            ResultSet rs = pst.executeQuery();
            EntityInfo ei = null;
            if (rs.next()) {
                ei = instantiateEntityInfo(rs);
            }
            rs.close();
            pst.close();
            commit(key);
            return ei;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public boolean deleteEntity(Long entity_id, Long user_id)
        throws SQLException
    {
        Integer key = -1;
        String sql = "SELECT * FROM \"Acidbase\".delete_entity(?, ?)";
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement(sql);

            pst.setLong(1, entity_id);
            pst.setLong(2, user_id);
            ResultSet rs = pst.executeQuery();

            boolean r = false;
            if (rs.next()) {
                r = true;
            }
            rs.close();
            pst.close();
            commit(key);
            return r;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public List<EntityInfo> getAllRevisionEntityInfos(Long entity_id)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            String query =
                "SELECT " +
                "   r.entity_id, r.object_id AS revision_id, e.entity_type, r.revision_state, e.create_date, e.creator, r.revise_date, r.reviser, r.fields " +
                "FROM \"Acidbase\".entity e " +
                "INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) " +
                "WHERE e.entity_id = ? " +
                "ORDER BY r.revise_date DESC "
                    ;

            PreparedStatement pst = con.prepareStatement(query);
            pst.setLong(1, entity_id);
            ResultSet rs = pst.executeQuery();
            ArrayList<EntityInfo> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(instantiateEntityInfo(rs));
            }
            rs.close();
            pst.close();
            commit(key);
            return entities;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public List<EntityInfo> getEntityInfoByType(String entity_type, ExtractionState state)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            String query =
                "SELECT " +
                "   e.entity_id, r.object_id AS revision_id, e.entity_type, r.revision_state, e.create_date, e.creator, r.revise_date, r.reviser, r.fields " +
                "FROM \"Acidbase\".entity e " +
                "INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) " +
                "WHERE e.entity_type = ? "
            ;

            if (state == ExtractionState.LatestActive) {
                query += " AND r.revision_state = 'Active' ";
            }
            else {
                query += " AND r.revision_state IN ('Active', 'Deleted') ";
            }

            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1, entity_type);
            ResultSet rs = pst.executeQuery();
            ArrayList<EntityInfo> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(instantiateEntityInfo(rs));
            }
            rs.close();
            pst.close();
            commit(key);
            return entities;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public List<EntityInfo> getRevisionInfoByType(String entity_type)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            String query =
                "SELECT " +
                "   e.entity_id, r.object_id AS revision_id, e.entity_type, r.revision_state, e.create_date, e.creator, r.revise_date, r.reviser, r.fields " +
                "FROM \"Acidbase\".entity e " +
                "INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) " +
                "WHERE e.entity_type = ? "
            ;

            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1, entity_type);
            ResultSet rs = pst.executeQuery();
            ArrayList<EntityInfo> entities = new ArrayList<>();
            while (rs.next()) {
                entities.add(instantiateEntityInfo(rs));
            }
            rs.close();
            pst.close();
            commit(key);
            return entities;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public boolean isRevisionPresent(Long revision_id)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();

            String query = "SELECT COUNT(*) AS count FROM \"Acidbase\".revision WHERE object_id = ?";

            PreparedStatement pst = con.prepareStatement(query);
            pst.setLong(1, revision_id);
            ResultSet rs = pst.executeQuery();
            Long count = 0l;
            if (rs.next()) {
                count = rs.getLong("count");
            }
            rs.close();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    static public EntityInfo instantiateEntityInfo(ResultSet rs)
        throws SQLException
    {
        JSONObject fields = new JSONObject();

        // In some cases `fields` might not be available
        //Let's check and see if it is available
        try {
            fields = (JSONObject) new JSONParser().parse(rs.getString("fields"));
        }
        catch (org.postgresql.util.PSQLException | ParseException ignored) {
            //Just discard the exception thrown and carry on
        }

        fields.put("entity_id", rs.getLong("entity_id"));
        fields.put("revision_id", rs.getLong("revision_id"));
        fields.put("entity_type", rs.getString("entity_type"));
        fields.put("create_date", rs.getString("create_date"));
        fields.put("revise_date", rs.getString("revise_date"));
        fields.put("creator", rs.getLong("creator"));
        fields.put("reviser", rs.getLong("reviser"));
        fields.put("revision_state", rs.getString("revision_state"));

        JSONObject eiJson = new JSONObject();
        eiJson.put("fields", fields);

        return EntityInfo.parse(eiJson.toString());
    }

    public EntityInfo lockEntity(Long entity_id, LockType lock_type)
        throws SQLException
    {
        EntityInfo ei = null;
        if (lock_type != LockType.NoLock) {
            String query = "SELECT * AS count FROM \"Acidbase\".entity e INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) WHERE e.entity_id = ? FOR " + (lock_type == LockType.Exclusive ? "UPDATE" : "SHARE");
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setLong(1, entity_id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                ei = instantiateEntityInfo(rs);
            }
            rs.close();
            pst.close();
        }
        else {
            ei = getRevisionEntityInfo(entity_id);
        }
        return ei;
    }

    public EntityInfo lockRevision(Long revision_id, LockType lock_type)
        throws SQLException
    {
        EntityInfo ei = null;
        if (lock_type != LockType.NoLock) {
            String query = "SELECT * AS count FROM \"Acidbase\".entity e INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) WHERE r.object_id = ? FOR " + (lock_type == LockType.Exclusive ? "UPDATE" : "SHARE");
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setLong(1, revision_id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                ei = instantiateEntityInfo(rs);
            }
            rs.close();
            pst.close();
        }
        else {
            ei = getRevisionEntityInfo(revision_id);
        }
        return ei;
    }

    public boolean editRevision(Long revision_id, String data, Long user_id)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            String sql = "UPDATE \"Acidbase\".revision SET reviser = ?, fields = ? WHERE object_id = ?";
            PreparedStatement pst = con.prepareStatement(sql);

            pst.setLong(1, user_id);
            PGobject pgo = new PGobject();
            pgo.setType("jsonb");
            pgo.setValue(data);
            pst.setObject(2, pgo);
            pst.setLong(3, revision_id);

            int count = pst.executeUpdate();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public Long getMaxRevisionId(String entityType)
        throws SQLException
    {
        Integer key = -1;

        try {
            key = beginTransaction();
            Connection ex = getConnection();
            String query;
            if (entityType == null) {
                query = "SELECT MAX(r.object_id) FROM \"Acidbase\".revision r";
            }
            else {
                query ="SELECT MAX(r.object_id) FROM \"Acidbase\".revision r INNER JOIN \"Acidbase\".entity e ON (r.entity_id = e.entity_id) WHERE e.entity_type = ?";
            }

            PreparedStatement pst = ex.prepareStatement(query);

            if(entityType != null) {
                pst.setString(1, entityType);
            }

            ResultSet rs = pst.executeQuery();
            Long maxId = 0l;

            if (rs.next()) {
                maxId = rs.getLong(1);
            }

            rs.close();
            pst.close();
            commit(key);
            return maxId;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public List<EntityInfo> listRevisions(String entity_type, Long start, Long limit)
        throws SQLException
    {
        Integer key = -1;

        try {
            key = beginTransaction();
            Connection ex = getConnection();
            String query = null;
            if(entity_type != null) {
                query = "SELECT e.entity_id, r.object_id AS revision_id, e.entity_type, r.revision_state, e.create_date, e.creator, r.revise_date, r.reviser, r.fields FROM \"Acidbase\".entity e INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) WHERE AND e.entity_type = ? ";
            }
            else {
                query = "SELECT e.entity_id, r.object_id AS revision_id, e.entity_type, r.revision_state, e.create_date, e.creator, r.revise_date, r.reviser, r.fields FROM \"Acidbase\".entity e INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) ";
            }

            if (start != null) {
                query = query + " LIMIT ? OFFSET ?";
            }

            PreparedStatement pst = ex.prepareStatement(query);
            int paramCounter = 0;
            if (entity_type != null) {
                pst.setString(++paramCounter, entity_type);
            }

            if (start != null) {
                pst.setLong(++paramCounter, limit);
                pst.setLong(++paramCounter, start);
            }

            ResultSet rs = pst.executeQuery();
            ArrayList entities = new ArrayList();

            while (rs.next()) {
                entities.add(EntityMapper.instantiateEntityInfo(rs));
            }

            rs.close();
            pst.close();
            this.commit(key);
            return entities;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public long countRevisions(String entity_type)
        throws SQLException
    {
        Integer key = -1;

        try {
            key = beginTransaction();
            Connection ex = getConnection();
            String query = null;
            if (entity_type != null) {
                query = "SELECT COUNT(*) AS count FROM \"Acidbase\".entity e INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) WHERE AND e.entity_type = ? ";
            }
            else {
                query = "SELECT COUNT(*) AS count FROM \"Acidbase\".entity e INNER JOIN \"Acidbase\".revision r ON (e.entity_id = r.entity_id) ";
            }

            PreparedStatement pst = ex.prepareStatement(query);
            if (entity_type != null) {
                pst.setString(1, entity_type);
            }

            ResultSet rs = pst.executeQuery();
            rs.next();
            long count = rs.getLong(1);
            rs.close();
            pst.close();
            commit(key);
            return count;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }
}
