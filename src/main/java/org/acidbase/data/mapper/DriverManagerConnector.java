package org.acidbase.data.mapper;

import org.acidbase.data.ServerInfo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DriverManagerConnector implements Connector
{
    private ServerInfo serverInfo;

    public DriverManagerConnector(ServerInfo serverInfo)
    {
        this.serverInfo = serverInfo;
    }

    @Override
    public String getIdentifier()
    {
        return serverInfo.getUrl();
    }

    @Override
    public Connection newConnection()
        throws SQLException
    {
        return DriverManager.getConnection(serverInfo.getUrl(), serverInfo.getUsername(), serverInfo.getPassword());
    }

    @Override
    public void close()
    {
        return;
    }

    @Override
    public boolean isClosed()
    {
        return true;
    }
}
