package org.acidbase.data.mapper;

import org.acidbase.data.IndexStructure;
import org.acidbase.data.IndexStructureException;
import org.acidbase.data.structure.ReadingStructure;
import org.acidbase.data.structure.WritingStructure;
import org.json.simple.parser.ParseException;
import org.postgresql.util.PGobject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Pattern;


public class IndexStructureMapper extends Mapper
{
    public IndexStructureMapper(Connector connector)
    {
        super(connector);
    }

    public Map<String, IndexStructure> getIndexStructure()
        throws SQLException, IndexStructureException, ParseException
    {
        List<Map<String, Object>> data = query("SELECT name, \"index\", read_structure, write_structure, version, script FROM \"Acidbase\".\"index_structure\" ORDER BY name");
        Map<String, IndexStructure> _indices = new TreeMap<>();

        for (Map<String, Object> rec : data) {
            IndexStructure is = new IndexStructure();
            is.name = rec.get("name").toString();
            is.index = (boolean) rec.get("index");
            is.version = (Integer) rec.get("version");
            is.script_name = rec.get("script") != null ? rec.get("script").toString() : null;
            String readingJson = rec.get("read_structure").toString();
            if (!readingJson.equals("")) {
                is.readingStructure = ReadingStructure.parse(readingJson);
            }
            String writingJson = rec.get("write_structure").toString();
            if (!writingJson.equals("")) {
                is.writingStructure = WritingStructure.parse(writingJson);
            }
            _indices.put(is.name, is);
        }
        return _indices;
    }

    public IndexStructure getIndexStructure(String index_name)
        throws SQLException, IndexStructureException, ParseException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("SELECT name, \"index\", read_structure, write_structure, version, script FROM \"Acidbase\".\"index_structure\" WHERE name = ?");
            pst.setString(1, index_name);
            ResultSet rs = pst.executeQuery();

            IndexStructure is = null;
            if (rs.next()) {
                is = new IndexStructure();
                is.name = rs.getString("name");
                is.index = rs.getBoolean("index");
                is.version = rs.getInt("version");
                is.script_name = rs.getString("script");
                String readingJson = rs.getString("read_structure");
                if (!readingJson.equals("")) {
                    is.readingStructure = ReadingStructure.parse(readingJson);
                }
                String writingJson = rs.getString("write_structure");
                if (!writingJson.equals("")) {
                    is.writingStructure = WritingStructure.parse(writingJson);
                }
            }

            rs.close();
            pst.close();
            commit(key);
            return is;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }

    }

    public boolean addIndexStructure(String name, boolean index, String script_name, ReadingStructure read_Writing_structure, WritingStructure write_Writing_structure)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("INSERT INTO \"Acidbase\".index_structure (name, \"index\", read_structure, write_structure, script) VALUES (?, ?, ?, ?, ?)");
            pst.setString(1, validateIndexName(name));
            pst.setBoolean(2, index);
            {
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(read_Writing_structure.toJson());
                pst.setObject(3, pgo);
            }
            {
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(write_Writing_structure.toJson());
                pst.setObject(4, pgo);
            }
            pst.setString(5, script_name);
            pst.executeUpdate();
            int count = pst.getUpdateCount();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public boolean removeIndexStructure(String name)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("DELETE FROM \"Acidbase\".index_structure WHERE name = ?");
            pst.setString(1, validateIndexName(name));
            pst.executeUpdate();
            int count = pst.getUpdateCount();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public boolean updateIndexStructure(String oldName, String newName, boolean index, String script_name, ReadingStructure read_Writing_structure, WritingStructure write_Writing_structure)
        throws SQLException
    {
        Integer key = -1;
        try {
            key = beginTransaction();
            Connection con = getConnection();
            PreparedStatement pst = con.prepareStatement("UPDATE \"Acidbase\".index_structure SET name = ?, \"index\" = ?, read_structure = ?, write_structure = ?, script = ? WHERE name = ?");
            pst.setString(1, validateIndexName(newName));
            pst.setBoolean(2, index);
            {
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(read_Writing_structure.toJson());
                pst.setObject(3, pgo);
            }
            {
                PGobject pgo = new PGobject();
                pgo.setType("jsonb");
                pgo.setValue(write_Writing_structure.toJson());
                pst.setObject(4, pgo);
            }
            pst.setString(5, script_name);
            pst.setString(6, validateIndexName(oldName));
            pst.executeUpdate();
            int count = pst.getUpdateCount();
            pst.close();
            commit(key);
            return count > 0;
        }
        catch (SQLException ex) {
            rollback(key);
            throw ex;
        }
    }

    public static String validateIndexName(String inx)
    {
        String character_sequence = Pattern.quote(".\\/*?\"<>|, ");
        String result = inx.replaceAll("[" + character_sequence + "]", "");
        return result.toLowerCase();
    }
}
