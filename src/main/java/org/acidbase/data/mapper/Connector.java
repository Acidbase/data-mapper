package org.acidbase.data.mapper;

import java.sql.Connection;
import java.sql.SQLException;

public interface Connector
{
    String getIdentifier();
    Connection newConnection() throws SQLException;
    void close();
    boolean isClosed();
}
