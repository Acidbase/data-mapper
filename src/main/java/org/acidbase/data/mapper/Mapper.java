package org.acidbase.data.mapper;

import org.acidbase.data.Command;
import org.acidbase.data.EntityInfo;

import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Mapper
{
    //region Structures

    public enum ExtractionState { LatestActiveOrRemoved, LatestActive }
    public enum LockType { NoLock, Shared, Exclusive }

    //endregion

    private static class ConnectionPackage
    {
        Connection connection;
        Integer transactionKey;
    }

    public enum ConnectionEvent
    {
        BEFORE_CONNECT,
        AFTER_CONNECT,
        AFTER_BEGIN,
        BEFORE_COMMIT,
        BEFORE_ROLLBACK,
        BEFORE_CLOSE
    }

    //region Static properties

    private static final ThreadLocal<Map<String, ConnectionPackage>> connections = new ThreadLocal<Map<String, ConnectionPackage>>() {
        @Override
        protected Map<String, ConnectionPackage> initialValue()
        {
            return new HashMap<>();
        }
    };
    private static final ConcurrentMap<ConnectionEvent, ConcurrentMap<String, Command>> eventCommands;

    static {
        eventCommands = new ConcurrentHashMap<>();
        eventCommands.put(ConnectionEvent.BEFORE_CONNECT, new ConcurrentHashMap<>());
        eventCommands.put(ConnectionEvent.AFTER_CONNECT, new ConcurrentHashMap<>());
        eventCommands.put(ConnectionEvent.AFTER_BEGIN, new ConcurrentHashMap<>());
        eventCommands.put(ConnectionEvent.BEFORE_COMMIT, new ConcurrentHashMap<>());
        eventCommands.put(ConnectionEvent.BEFORE_ROLLBACK, new ConcurrentHashMap<>());
        eventCommands.put(ConnectionEvent.BEFORE_CLOSE, new ConcurrentHashMap<>());
    }
    
    //endregion

    protected String connectionName;
    protected Connector connector;
    private List<String> alreadyInBegin;
    private List<String> alreadyInCommit;
    private List<String> alreadyInRollback;
    private List<String> alreadyInClose;
    private List<String> alreadyInConnect;

    public Mapper(Connector connector)
    {
        this.connector = connector;
        connectionName = connector.getIdentifier();

        alreadyInBegin = new ArrayList<>();
        alreadyInCommit = new ArrayList<>();
        alreadyInRollback = new ArrayList<>();
        alreadyInClose = new ArrayList<>();
        alreadyInConnect = new ArrayList<>();
    }

    //region Connection methods

    protected void connect()
        throws SQLException
    {
        Map<String, ConnectionPackage> connections = getConnections();
        List<String> alreadyIn = alreadyInConnect;
        if (!alreadyIn.contains(connectionName)) {
            alreadyIn.add(connectionName);

            try {
                if (connections.containsKey(connectionName) && connections.get(connectionName).connection.isClosed()) {
                    connections.remove(connectionName);
                }
            }
            catch (SQLException ignored) {
                connections.remove(connectionName);
            }

            Connection con = null;
            try {
                if (!connections.containsKey(connectionName)) {
                    executeCommandsOnPackage(ConnectionEvent.BEFORE_CONNECT);
                    ConnectionPackage pkg = new ConnectionPackage();
                    con = pkg.connection = connector.newConnection();
                    pkg.connection.setAutoCommit(true);
                    connections.put(connectionName, pkg);
                    executeCommandsOnPackage(ConnectionEvent.AFTER_CONNECT);
                }
            }
            catch (SQLException ex) {
                try {
                    if (con != null && !con.isClosed()) {
                        con.close();
                    }
                }
                catch (SQLException ignored) {}
                if (connections.containsKey(connectionName)) {
                    connections.remove(connectionName);
                }
                throw ex;
            }
            finally {
                alreadyIn.remove(connectionName);
            }
        }
    }

    public void close()
        throws SQLException
    {
        Map<String, ConnectionPackage> connections = getConnections();
        List<String> alreadyIn = alreadyInClose;
        if (!alreadyIn.contains(connectionName)) {
            alreadyIn.add(connectionName);

            ConnectionPackage pkg = null;
            try {
                if (connections.containsKey(connectionName)) {
                    pkg = connections.get(connectionName);
                    if (pkg.transactionKey != null) {
                        executeCommandsOnPackage(ConnectionEvent.BEFORE_COMMIT);
                        pkg.connection.commit();
                    }
                    executeCommandsOnPackage(ConnectionEvent.BEFORE_CLOSE);
                    pkg.connection.close();
                }
            }
            catch (SQLException ex) {
                throw ex;
            }
            finally {
                if (pkg != null) {
                    try {
                        if (!pkg.connection.isClosed()) {
                            pkg.connection.close();
                        }
                    }
                    catch (SQLException ignored) {}
                    pkg.connection = null;
                    connections.remove(connectionName);
                }
                alreadyIn.remove(connectionName);
            }
        }
    }

    protected Map<String, ConnectionPackage> getConnections()
    {
        return connections.get();
    }

    public Connection getConnection()
        throws SQLException
    {
        Map<String, ConnectionPackage> connections = getConnections();
        connect();
        if (connections.containsKey(connectionName)) {
            ConnectionPackage pkg = connections.get(connectionName);
            return pkg.connection;
        }
        return null;
    }

    //endregion

    //region Transaction method

    public Integer beginTransaction()
        throws SQLException
    {
        Map<String, ConnectionPackage> connections = getConnections();
        Integer k = -1;
        List<String> alreadyIn = alreadyInBegin;
        if (!alreadyIn.contains(connectionName)) {
            alreadyIn.add(connectionName);

            try {
                connect();
                ConnectionPackage pkg = connections.get(connectionName);
                if (pkg.transactionKey == null) {
                    pkg.transactionKey = ThreadLocalRandom.current().nextInt(0, Integer.MAX_VALUE);
                    pkg.connection.setAutoCommit(false);
                    executeCommandsOnPackage(ConnectionEvent.AFTER_BEGIN);
                    k = pkg.transactionKey;
                }
            }
            catch (SQLException ex) {
                throw ex;
            }
            finally {
                alreadyIn.remove(connectionName);
            }
        }
        return k;
    }

    public boolean commit(Integer key)
        throws SQLException
    {
        if (key > -1) {
            Map<String, ConnectionPackage> connections = getConnections();
            List<String> alreadyIn = alreadyInCommit;
            if (!alreadyIn.contains(connectionName)) {
                alreadyIn.add(connectionName);
                boolean r = false;

                ConnectionPackage pkg = null;
                try {
                    if (connections.containsKey(connectionName)) {
                        pkg = connections.get(connectionName);
                        if (pkg.transactionKey != null
                        && pkg.transactionKey.equals(key)) {
                            executeCommandsOnPackage(ConnectionEvent.BEFORE_COMMIT);
                            pkg.connection.commit();
                            r = true;
                        }
                    }
                }
                catch (SQLException ex) {
                    throw ex;
                }
                finally {
                    if (pkg != null
                    && pkg.transactionKey != null
                    && pkg.transactionKey.equals(key)) {
                        pkg.transactionKey = null;
                    }
                    alreadyIn.remove(connectionName);
                }

                return r;
            }
        }
        return false;
    }
    
    public boolean rollback(Integer key)
        throws SQLException
    {
        if (key != -1) {
            Map<String, ConnectionPackage> connections = getConnections();
            List<String> alreadyIn = alreadyInRollback;
            if (!alreadyIn.contains(connectionName)) {
                boolean r = false;
                alreadyIn.add(connectionName);

                ConnectionPackage pkg = null;
                try {
                    if (connections.containsKey(connectionName)) {
                        pkg = connections.get(connectionName);
                        if (pkg.transactionKey != null
                        && pkg.transactionKey.equals(key)) {
                            executeCommandsOnPackage(ConnectionEvent.BEFORE_ROLLBACK);
                            pkg.connection.rollback();
                            r = true;
                        }
                    }
                }
                catch (SQLException ex) {
                    throw ex;
                }
                finally {
                    if (pkg != null
                    && pkg.transactionKey != null
                    && pkg.transactionKey.equals(key)) {
                        pkg.transactionKey = null;
                    }
                    alreadyIn.remove(connectionName);
                }

                return r;
            }
        }
        return false;
    }

    //endregion
    
    //region Generic methods

    public List<Map<String, Object>> query(String query)
        throws SQLException
    {
        return query(query, true);
    }

    public List<Map<String, Object>> query(String query, boolean insideTransaction)
        throws SQLException
    {
        Integer key = -1;
        try {
            if (insideTransaction) {
                key = beginTransaction();
            }
            else {
                connect();
            }
            Connection c = getConnection();
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            ResultSetMetaData meta = rs.getMetaData();
            final int columnCount = meta.getColumnCount();
            List<Map<String, Object>> data = new ArrayList<>();
            while (rs.next()) {
                Map<String, Object> record = new HashMap<>();
                for (int cn=1; cn <= columnCount; cn++) {
                    record.put(meta.getColumnName(cn), rs.getObject(cn));
                }
                data.add(record);
            }
            rs.close();
            stmt.close();
            if (insideTransaction) {
                commit(key);
            }
            return data;
        }
        catch (SQLException ex) {
            if (insideTransaction) {
                rollback(key);
            }
            throw ex;
        }
    }

    public Integer execute(String sql)
        throws SQLException
    {
        return execute(sql, true);
    }

    public Integer execute(String sql, boolean insideTransaction)
        throws SQLException
    {
        Integer key = -1;
        try {
            if (insideTransaction) {
                key = beginTransaction();
            }
            else {
                connect();
            }
            Connection c = getConnection();
            Statement stmt = c.createStatement();
            stmt.executeUpdate(sql);
            int count = stmt.getUpdateCount();
            if (insideTransaction) {
                commit(key);
            }
            return count;
        }
        catch (SQLException ex) {
            if (insideTransaction) {
                rollback(key);
            }
            throw ex;
        }
    }
    
    private void executeCommandsOnPackage(ConnectionEvent e)
        throws SQLException
    {
        List<Command> commands = eventCommands
            .get(e)
            .entrySet()
            .stream()
            .filter(entry -> entry.getKey().startsWith(connectionName + ".") || entry.getKey().startsWith("."))
            .map(Map.Entry::getValue)
            .collect(Collectors.toList());

        if (commands.size() > 1) {
            Collections.sort(commands, (x, y) -> x.getPriority().compareTo(y.getPriority()));
        }
        for (Command c : commands) {
            c.execute(this);
        }
    }
    
    public String extractionState2String(ExtractionState state)
    {
        switch (state) {
            case LatestActiveOrRemoved:
                return "Latest";
                
            case LatestActive:
                return "LatestActive";
        }
        
        return null;
    }

    public String lockType2String(LockType type)
    {
        switch (type) {
            case NoLock:
                return "NoLock";

            case Shared:
                return "Shared";

            case Exclusive:
                return "Exclusive";
        }
        
        return null;
    }

    static public EntityInfo.EntityState string2RevisionState(String str)
    {
        switch (str) {
            case "Active":
                return EntityInfo.EntityState.Active;
            case "Obsolete":
                return EntityInfo.EntityState.Obsolete;
            case "Deleted":
                return EntityInfo.EntityState.Deleted;
            case "Exdeleted":
                return EntityInfo.EntityState.Revived;
        }

        return null;
    }

    public void obtainSessionLock(long id)
        throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement pst = c.prepareStatement("SELECT pg_advisory_lock(?)");
        pst.setLong(1, id);
        pst.executeQuery().close();
        pst.close();
    }

    public void releaseSessionLock(long id)
        throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement pst = c.prepareStatement("SELECT pg_advisory_unlock(?)");
        pst.setLong(1, id);
        pst.executeQuery().close();
        pst.close();
    }

    public boolean trySessionLock(long id)
        throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement pst = c.prepareStatement("SELECT pg_try_advisory_lock(?) AS locked");
        pst.setLong(1, id);
        ResultSet rs = pst.executeQuery();
        boolean locked = false;
        if (rs.next()) {
            locked = rs.getBoolean("locked");
        }
        rs.close();
        pst.close();
        return locked;
    }

    public void obtainTransactionLock(long id)
        throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement pst = c.prepareStatement("SELECT pg_advisory_xact_lock(?)");
        pst.setLong(1, id);
        pst.executeQuery().close();
        pst.close();
    }

    public boolean tryTransactionLock(long id)
        throws SQLException
    {
        Connection c = getConnection();
        PreparedStatement pst = c.prepareStatement("SELECT pg_try_advisory_xact_lock(?) AS locked");
        pst.setLong(1, id);
        ResultSet rs = pst.executeQuery();
        boolean locked = false;
        if (rs.next()) {
            locked = rs.getBoolean("locked");
        }
        rs.close();
        pst.close();
        return locked;
    }

    //endregion
    
    //region Command methods
    
    public static boolean registerCommand(ConnectionEvent e, String connectionName, String commandName, Command command)
    {
        return eventCommands.get(e).putIfAbsent(connectionName + "." + commandName, command) == command;
    }

    public static boolean registerCommand(ConnectionEvent e, String commandName, Command command)
    {
        return registerCommand(e, "", commandName, command);
    }

    public static boolean unregisterCommand(ConnectionEvent e, String connectionName, String commandName)
    {
        ConcurrentMap<String, Command> cmd = eventCommands.get(e);
        if (cmd.containsKey(connectionName + "." + commandName)) {
            cmd.remove(connectionName + "." + commandName);
            return true;
        }
        else
            return false;
    }

    public static boolean unregisterCommand(ConnectionEvent e, String commandName)
    {
        return unregisterCommand(e, "", commandName);
    }

    //endregion
}