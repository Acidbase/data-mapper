package org.acidbase.data;

public class ServerInfo
{
    protected String protocol;
    protected String type;
    protected String host;
    protected int port;
    protected String database;
    protected String username;
    protected String password;

    public ServerInfo(
        String protocol,
        String type,
        String host,
        int port,
        String database,
        String username,
        String password
    )
    {
        this.protocol = protocol;
        this.type = type;
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
    }

    public String getProtocol()
    {
        return protocol;
    }

    public String getType()
    {
        return type;
    }

    public String getHost()
    {
        return host;
    }

    public int getPort()
    {
        return port;
    }

    public String getDatabase()
    {
        return database;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public String getUrl()
    {
        return formatUrl(protocol, type, host, port, database);
    }

    public String getUrl(String database)
    {
        return formatUrl(protocol, type, host, port, database);
    }

    public static String formatUrl(String protocol, String type, String host, int port, String database)
    {
        return String.format("%s:%s://%s:%d/%s", protocol, type, host, port, database);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) {
            return false;
        }
        if (!ServerInfo.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final ServerInfo serverInfo = (ServerInfo) obj;
        return this.getUrl().equals(serverInfo.getUrl());
    }

    @Override
    public int hashCode()
    {
        return getUrl().hashCode();
    }

    public ServerInfo cloneWithNewDb(String database)
    {
        return new ServerInfo(
            protocol,
            type,
            host,
            port,
            database,
            username,
            password
        );
    }

    public ServerInfo clone()
    {
        return new ServerInfo(
            protocol,
            type,
            host,
            port,
            database,
            username,
            password
        );
    }
}
