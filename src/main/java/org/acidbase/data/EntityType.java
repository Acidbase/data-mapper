package org.acidbase.data;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EntityType
{
    private String name;
    private Map<String, String> fields;

    public EntityType() {}

    public EntityType(String name, Map<String, String> fields)
    {
        this.name = name;
        this.fields = fields;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Map<String, String> getFields()
    {
        return fields;
    }

    public void setFields(Map<String, String> fields)
    {
        this.fields = fields;
    }

    //region Factory static method

    static public Map<String, String> parseFields(String json)
        throws ParseException
    {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(json);
        Map<String, String> fields = new HashMap<>();

        for (Iterator iterator = obj.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            fields.put(key, (String) obj.get(key));
        }

        return fields;
    }

    static public String stringifyFields(Map<String, String> entity_fields)
    {
        JSONObject obj = new JSONObject(entity_fields);
        return obj.toString();
    }

    //endregion
}
