package org.acidbase.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Entity extends EntityInfo
{
    static Logger logger = LoggerFactory.getLogger(Entity.class);
    public Long object_id;
    public Map<String, List<Entity>> collections = new HashMap<>();

    public Entity() {}

    static public Entity parse(String json)
    {
        ObjectMapper jsonMapper = new ObjectMapper();
        try {
            return jsonMapper.readValue(json, Entity.class);
        }
        catch (IOException e) {
            logger.error(e.toString());
        }
        return null;
    }

    public String stringifyForDb()
    {
        return toJsonObject().toString();
    }

    public JSONObject toJsonObject()
    {
        JSONObject obj = new JSONObject();
        if (entity_id != null && entity_id > 0) {
            obj.put("entity_id", entity_id);
        }
        if (revision_id != null && revision_id > 0) {
            obj.put("revision_id", revision_id);
        }
        if (object_id != null && object_id > 0) {
            obj.put("object_id", object_id);
        }
        obj.put("fields", fields);

        JSONObject collections = new JSONObject();
        for (Map.Entry<String, List<Entity>> entry : this.collections.entrySet()) {
            JSONArray collection = new JSONArray();
            for (Entity e : entry.getValue()) {
                collection.add(e.toJsonObject());
            }
            collections.put(entry.getKey(), collection);
        }
        obj.put("collections", collections);

        return obj;
    }
}
