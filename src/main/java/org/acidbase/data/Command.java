package org.acidbase.data;

import org.acidbase.data.mapper.Mapper;

import java.sql.SQLException;

public abstract class Command
{
    private Integer priority;
    
    public Command(Integer _priority)
    {
        priority = _priority;
    }
    
    public Integer getPriority()
    {
        return priority;
    }
    
    public abstract void execute(Mapper mapper) throws SQLException;
}
