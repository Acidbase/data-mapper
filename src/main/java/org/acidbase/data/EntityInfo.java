package org.acidbase.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class EntityInfo
{
    public enum EntityState { Active, Obsolete, Deleted, Revived }

    static Logger logger = LoggerFactory.getLogger(EntityInfo.class);
    public Long entity_id = null;
    public Long revision_id = null;
    public String entity_type = null;
    public EntityState revision_state = null;
    public Timestamp create_date = null;
    public Timestamp revise_date = null;
    public Long creator = null;
    public Long reviser = null;
    public Map<String, Object> fields = new HashMap<>();

    public <T extends Object> T field(String name)
    {
        Object f = null;
        if (fields != null) {
            f = fields.get(name);
        }

        return (T) f;
    }

    public void field(String name, Object value)
    {
        fields.put(name, value);
    }

    static public EntityInfo parse(String json)
    {
        ObjectMapper jsonMapper = new ObjectMapper();
        try {
            return jsonMapper.readValue(json, EntityInfo.class);
        }
        catch (IOException e) {
            logger.error(e.toString());
        }
        return null;
    }
}