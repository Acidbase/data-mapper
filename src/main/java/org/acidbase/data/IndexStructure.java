package org.acidbase.data;

import org.acidbase.data.structure.ReadingStructure;
import org.acidbase.data.structure.Structure;
import org.acidbase.data.structure.WritingStructure;

public class IndexStructure
{
    public enum ActionType {
        Add,
        Revise,
        Delete,
        Revive,
        Edit,
        Purge
    }

    public String name;
    public boolean index = false;
    public ReadingStructure readingStructure;
    public WritingStructure writingStructure;
    public Integer version;
    public String script_name;

    public IndexStructure() {}

    public boolean isReindexingNecessary(String type, ActionType actionType)
    {
        switch (actionType) {
            case Add:
                if (readingStructure.entity_type == type) {
                    return true;
                }
                else {
                    return readingStructure.includesEntityType(type, new Structure.RevisionStateType[] {
                        Structure.RevisionStateType.LatestActive,
                        Structure.RevisionStateType.Latest
                    });
                }

            case Revise:
                return readingStructure.includesEntityType(type, new Structure.RevisionStateType[] {
                    Structure.RevisionStateType.LatestActive,
                    Structure.RevisionStateType.Latest
                });

            case Revive:
            case Delete:
                return readingStructure.includesEntityType(type, new Structure.RevisionStateType[] {
                    Structure.RevisionStateType.LatestActive
                });

            case Edit:
            case Purge:
                return readingStructure.includesEntityType(type, new Structure.RevisionStateType[] {
                    Structure.RevisionStateType.Revision,
                    Structure.RevisionStateType.Latest,
                    Structure.RevisionStateType.LatestActive
                });
        }

        return false;
    }
}
