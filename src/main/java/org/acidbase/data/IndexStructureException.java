package org.acidbase.data;

public class IndexStructureException extends Exception
{
    public IndexStructureException(String error)
    {
        super(error);
    }
}
