package org.acidbase.data.structure;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class ReadingStructure extends Structure
{
    static Logger logger = LoggerFactory.getLogger(ReadingStructure.class);
    public List<String> fields = new ArrayList<>();
    public Map<String, ReadingStructure> collections = new HashMap<>();

    public boolean includesEntityType(String type, Structure.RevisionStateType[] states)
    {
        if (entity_type == type && Arrays.asList(states).contains(revision_state)) {
            return true;
        }

        for (Map.Entry<String, ReadingStructure> entry : collections.entrySet()) {
            if (entry.getValue().includesEntityType(type, states)) {
                return true;
            }
        }

        return false;
    }

    static public ReadingStructure parse(String json)
    {
        ObjectMapper jsonMapper = new ObjectMapper();
        try {
            return jsonMapper.readValue(json, ReadingStructure.class);
        }
        catch (IOException e) {
            logger.error(e.toString());
        }
        return null;
    }
}
