package org.acidbase.data.structure;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WritingStructure extends Structure
{
    static Logger logger = LoggerFactory.getLogger(WritingStructure.class);
    public Map<String, String> fields = new HashMap<>();
    public Map<String, WritingStructure> collections = new HashMap<>();

    static public WritingStructure parse(String json)
    {
        ObjectMapper jsonMapper = new ObjectMapper();
        try {
            return jsonMapper.readValue(json, WritingStructure.class);
        }
        catch (IOException e) {
            logger.error(e.toString());
        }
        return null;
    }
}
