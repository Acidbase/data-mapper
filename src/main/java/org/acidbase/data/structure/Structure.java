package org.acidbase.data.structure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Structure
{
    static Logger logger = LoggerFactory.getLogger(Structure.class);
    public enum RevisionStateType {
        LatestActive,   //Used when entity revised / revived / removed - 0
        Latest,         //Used when entity revised - 0,2
        Revision,       //Used when an UPDATE or DELETE SQL is executed
        NotAnEntity
    }
    public RevisionStateType revision_state;
    public String entity_type;

    public String toJson()
    {
        ObjectMapper jsonMapper = new ObjectMapper();
        try {
            return jsonMapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e) {
            logger.error(e.toString());
        }
        return null;
    }
}
