package org.acidbase.data;

import org.acidbase.data.mapper.Connector;
import org.acidbase.data.mapper.IndexStructureMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class IndexStructureCache
{
    private static final RemovalListener<Connector, IndexStructureCache> removalListener;
    private static final LoadingCache<Connector, IndexStructureCache> instances;

    static {
        removalListener = removal -> {};

        instances = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .removalListener(removalListener)
            .build(
                new CacheLoader<Connector, IndexStructureCache>() {
                    public IndexStructureCache load(Connector connector)
                        throws ParseException, IndexStructureException, SQLException, IOException
                    {
                        return new IndexStructureCache(connector);
                    }
            });
    }

    public static IndexStructureCache getInstance(Connector connector)
        throws ExecutionException
    {
        synchronized (instances) {
            return instances.get(connector);
        }
    }

    public static void invalidateInstance(Connector connector)
    {
        synchronized (instances) {
            instances.invalidate(connector);
        }
    }

    private Map<String, IndexStructure> indices;
    protected Connector connector;

    public Map<String, IndexStructure> getIndexStructures()
    {
        return indices;
    }

    public IndexStructure getIndexStructure(String name)
    {
        if (indices.containsKey(name)) {
            return indices.get(name);
        }
        else {
            return null;
        }
    }

    private IndexStructureCache(Connector connector)
        throws ParseException, IndexStructureException, SQLException, IOException
    {
        this.connector = connector;
        reloadIndices();
    }

    private void reloadIndices()
        throws SQLException, IndexStructureException, ParseException, IOException
    {
        IndexStructureMapper mapper = new IndexStructureMapper(connector);
        indices = mapper.getIndexStructure();
        mapper.close();
    }
}
